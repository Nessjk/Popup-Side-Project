import Vue from 'vue'
import App from './App'
import * as firebase from 'firebase'
import router from './router'
import Vuex from 'vuex'
import { store } from './store'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import { Photoshop, Chrome } from 'vue-color'
import Vuelidate from 'vuelidate'

// Shared components
import goBackButton from '@/components/shared/goBackButton.vue'
import loading from '@/components/shared/loading.vue'
import alert from '@/components/shared/alert.vue'


// Main components
import NameAndColors1 from '@/components/NameAndColors1.vue'
import FontSelection2 from '@/components/FontSelection2.vue'
import BuildStep3 from '@/components/BuildStep3.vue'

// Dashboard
import Dashboard from '@/components/popups.vue'

// Form imports
import Preview from '@/components/form/preview.vue'
import PreviewFooter from '@/components/form/footer.vue'

// Sidebar import
import Progress from '@/components/sidebar/progress.vue'

// Preview fields
import PreviewField1 from '@/components/form/fields/fields1.vue'
import PreviewField2 from '@/components/form/fields/fields2.vue'
import PreviewField3 from '@/components/form/fields/fields3.vue'
import PreviewField4 from '@/components/form/fields/fields4.vue'
import Submitted from '@/components/form/fields/submitted.vue'

// Advanced fields
import SubtitleColor from '@/components/form/fields/FieldsAdvanced/subtitleColor.vue'
import ButtonColor from '@/components/form/fields/FieldsAdvanced/buttonColor.vue'

// Event Bus
export const EventBus = new Vue();

Vue.use(Vuetify)
Vue.use(Vuelidate)
Vue.component('app-comp-1', NameAndColors1)
Vue.component('app-comp-2', FontSelection2)
Vue.component('app-comp-3', BuildStep3)
Vue.component('app-preview', Preview)
Vue.component('app-footer', PreviewFooter)
Vue.component('app-progress', Progress)
Vue.component('app-fields-1', PreviewField1)
Vue.component('app-fields-2', PreviewField2)
Vue.component('app-fields-3', PreviewField3)
Vue.component('app-fields-4', PreviewField4)
Vue.component('app-submitted', Submitted)
Vue.component('dashboard', Dashboard)
Vue.component('app-loading', loading)

Vue.component('chrome', Chrome )
Vue.component('subtitle-color', SubtitleColor )
Vue.component('button-color', ButtonColor )

Vue.component('go-back-button', goBackButton)
Vue.component('show-alert', alert)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  created() {
    firebase.initializeApp({
      apiKey: "AIzaSyD6qEcGUkBUyh2-SLqu9CvX6NXo7OYF4iI",
      authDomain: "popup-maker-676a9.firebaseapp.com",
      databaseURL: "https://popup-maker-676a9.firebaseio.com",
      projectId: "popup-maker-676a9",
      storageBucket: "popup-maker-676a9.appspot.com",
      messagingSenderId: "327114757506"
    });
  this.$store.dispatch('loadPopups')

  }
})

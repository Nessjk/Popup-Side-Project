import Vue from 'vue'
import Router from 'vue-router'
import Popup from '@/components/popup.vue'
import Popups from '@/components/popups.vue'

Vue.use(Router)

export default new Router({
  routes: [  
    {
      path: '/popups',
      name: 'Popups',
      component: Popups
    },
    {
      path: '/popups/:id',
      name: 'Popup',
      props: true,
      component: Popup
    },
  ],
  mode: 'history'
})

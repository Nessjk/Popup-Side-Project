import Vue from 'vue'
import Vuex from 'vuex'
import * as firebase from 'firebase'
import { stat } from 'fs';

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    loading: false,
    error: null,
    name:'',
    currentStep: 0,
    currentPreviewStep: 0,
    currentPopupType: 'Center',
    popupPosition: null,
    PopUpAnimation: null,
    PopUpTitle: null,
    popUpSubTitle: null,
    popupBtnText: null,
    textField1: null,
    textField2: null,
    showTextField1: true,
    showTextField2: false,
    popupBackgroundColor: 'rgb(0, 16, 52)',
    advancedSelected: false,
    popupBtnTextColor: '#FFFFFF',
    popupBtnColor: '#00cc47',
    popUpSubTitleColor: '#FFFFFF',
    popUpTitleColor: '#FFFFFF',
    selectedImage: null,
    imageNotes: null,
    browserPageBg: '#0065ff',
    popupDisplayTiming: null,
    finishingTouchesNotes: null,
    overlayBg: null,
    websiteUrl: null,
    imageUrl: null,
    selectedVibe: null,
    selectedFontStyle: null,
    showErrorTiming: false,
    showErrorUrl: false,
    loadedPopups:[]
  },
  mutations: {
      setName (state, payload) {
        state.name = payload
      },
      nextStep (state) {
        //console.log(state.currentStep)
        state.currentStep += 1
        //console.log(state.currentStep)
      },
      previousStep (state) {
        //console.log(state.currentStep)
        state.currentStep -= 1
        //console.log(state.currentStep)
      },
      nextPreviewStep (state) {
        state.currentPreviewStep += 1
        //console.log(state.currentPreviewStep)
      },
      previousPreviewStep (state) {
        state.currentPreviewStep -= 1
        //console.log(state.currentPreviewStep)
        // Must reset the Text Fields when clicking on previous
        //state.showTextField1 = true
        //state.showTextField2 = false
      },
      selectedPopupType (state, payload) {
        state.currentPopupType = payload
      },
      setPopUpPosition (state, payload) {
        state.popupPosition = payload
      },
      setPopUpAnimation (state, payload) {
        state.PopUpAnimation = payload
      },
      setPopUpTitle (state, payload) {
        state.PopUpTitle = payload
      },
      setPopUpBtnText (state,payload) {
        state.popupBtnText = payload
      },
      setPopUpBtnTextColor (state, payload) {
        state.popupBtnTextColor = payload
      },
      setPopUpBtnColor (state, payload) {
        state.popupBtnColor = payload
      },
      setSubTitleColor (state, payload) {
        state.popUpSubTitleColor = payload
      },
      setPopUpTitleColor (state, payload) {
        state.popUpTitleColor = payload
      },
      setTextfield1 (state, payload) {
        state.textField1 = payload
      },
      setTextfield2 (state, payload) {
        state.textField2 = payload
      },
      setPopUpSubTitle (state, payload) {
        state.popUpSubTitle = payload
      },
      resetPopUpPosition (state) {
        state.popupPosition = null
      },
      toggleTextField1 (state) {
        state.showTextField1 = !state.showTextField1
      },
      toggleTextField2 (state) {
        state.showTextField2 = !state.showTextField2
      },
      setpopupBackgroundColor (state, payload) {
        state.popupBackgroundColor = payload
      },
      toggleAdvancedSelected (state, payload) {
        state.advancedSelected = payload
      },
      setSelectedImage (state, payload) {
        state.selectedImage = payload
      },
      setImageNotes (state, payload) {
        state.imageNotes = payload
      },
      resetAllColors (state) {
        state.popupBackgroundColor = 'rgb(0, 16, 52)'
        state.popupBtnTextColor = '#FFFFFF'
        state.popupBtnColor = '#00cc47'
        state.popUpSubTitleColor = '#FFFFFF'
        state.popUpTitleColor = '#FFFFFF'
        state.browserPageBg = '#0065ff'
      },
      setBrowserPageBg (state, payload) {
        state.browserPageBg = payload
      },
      setPopupDisplayTiming (state, payload) {
        state.popupDisplayTiming = payload
        state.showErrorTiming = false
      },
      setFinishingTouchesNotes (state, payload) {
        state.finishingTouchesNotes = payload
      },
      setOverlayBg (state, payload) {
        state.overlayBg = payload
      },
      setWebsiteUrl(state, payload) {
          state.websiteUrl = payload
          state.showErrorUrl = false
      },
      setImageUrl(state, payload) {
          state.imageUrl = payload
      },
      setLoadedPopUps (state, payload) {
        state.loadedPopups = payload
      },
      setLoading(state) {
        state.loading = !state.loading
      },
      setUsername(state, payload) {
        state.name = payload
      },
      setSelectedVibe(state, payload) {
        state.selectedVibe = payload
      },
      setSelectedFontStyle(state, payload) {
        state.selectedFontStyle = payload
      },
      setShowErrorTiming (state, payload) {
        state.showErrorTiming = payload
      },
      setShowErrorUrl (state, payload) {
        state.showErrorUrl = payload
      }
      
  },
  actions: {
      setName (state, payload) {
        commit('setUser', payload)
      },
      nextStep ({commit}, state) {
        commit('nextStep')
      },
      previousStep ({commit}, state) {
        commit('previousStep')
      },
      nextPreviewStep ({commit}, state) {
        commit('nextPreviewStep')
      },
      previousPreviewStep ({commit}, state) {
        commit('previousPreviewStep')
      },
      selectedPopupType ({commit}, payload) {
        commit('selectedPopupType', payload)
        //commit('resetPopUpPosition')
      },
      setPopUpPosition ({commit}, payload) {
        commit('setPopUpPosition', payload)
      },
      setPopUpAnimation ({commit}, payload) {
        commit('setPopUpAnimation', payload)
      },
      // Button Options
      setPopUpTitle ({commit}, payload) {
        commit('setPopUpTitle', payload)
      },
      setPopUpBtnText ({commit}, payload) {
        commit('setPopUpBtnText', payload)
      },
      setPopUpBtnTextColor ({commit}, payload) {
        commit('setPopUpBtnTextColor', payload)
      },
      setPopUpBtnColor ({commit}, payload) {
        commit('setPopUpBtnColor', payload)
      },
      setSubTitleColor ({commit}, payload) {
        commit('setSubTitleColor', payload)
      },
      setPopUpTitleColor ({commit}, payload) {
        commit('setPopUpTitleColor', payload)
      },
      // Text Field Options
      setTextfield1 ({commit}, payload) {
        commit('setTextfield1', payload)
      },
      setTextfield2 ({commit}, payload) {
        commit('setTextfield2', payload)
      },
      setPopUpSubTitle ({commit}, payload) {
        commit('setPopUpSubTitle', payload)
      },
      // Add/delete text fields
      toggleTextField1 ({commit}) {
        commit('toggleTextField1')
      },
      toggleTextField2 ({commit}) {
        commit('toggleTextField2')
      },
      popupBackgroundColor ({commit}, payload) {
        commit('setpopupBackgroundColor', payload)
      },
      // Toggle advanced
      advancedSelected ({commit}, payload) {
          commit('toggleAdvancedSelected', payload)
      },
      setSelectedImage ({commit}, payload) {
          commit('setSelectedImage', payload)
      },
      imageNotes ({commit}, payload) {
          commit('setImageNotes', payload)
      },
      resetAllColors ({commit}) {
          commit('resetAllColors')
      },
      browserPageBg ({commit}, payload) {
        commit('setBrowserPageBg', payload)
      },
      popupDisplayTiming ({commit}, payload) {
         commit('setPopupDisplayTiming', payload)
      },
      finishingTouchesNotes ({commit}, payload) {
          commit('setFinishingTouchesNotes', payload)
      },
      overlayBg ({commit}, payload) {
          commit('setOverlayBg', payload)
      },
      websiteUrl ({commit}, payload) {
          commit('setWebsiteUrl', payload)
      },
      imageUrl({commit}, payload) {
          commit('setImageUrl', payload)
      },
      createPopUp({state, commit}, payload) {
        commit('setLoading')
        commit('nextPreviewStep')

        const newPopUp = {
          popupPosition: payload.popupPosition,
          PopUpTitle: payload.PopUpTitle,
          popUpSubTitle: payload.popUpSubTitle,
          popupBtnText: payload.popupBtnText,
          textField1: payload.textField1,
          textField2: payload.textField2,
          showTextField1: payload.showTextField1,
          showTextField2: payload.showTextField2,
          popupBackgroundColor: payload.popupBackgroundColor,
          advancedSelected: payload.advancedSelected,
          advancedSelected: payload.advancedSelected,
          popupBtnTextColor: payload.popupBtnTextColor,
          popupBtnColor: payload.popupBtnColor,
          popUpSubTitleColor: payload.popUpSubTitleColor,
          popUpTitleColor: payload.popUpTitleColor,
          selectedImage: payload.selectedImage,
          imageNotes: payload.imageNotes,
          browserPageBg: payload.browserPageBg,
          popupDisplayTiming: payload.popupDisplayTiming,
          finishingTouchesNotes: payload.finishingTouchesNotes,
          overlayBg: payload.overlayBg,
          websiteUrl: payload.websiteUrl,
          imageUrl: payload.imageUrl,
          date: new Date().toISOString()
      }
      const newUser = {
        websiteUrl: payload.websiteUrl
      }

      firebase.database().ref('clients').push(newUser)
        .catch((error) => {
          console.log(error)
      })
      
      firebase.database().ref('popups').push(newPopUp)
        .catch((error) => {
          console.log(error)
      })
      console.log('popup created')
      commit('setLoading')
        
      },
      loadPopups({commit}) {
          firebase.database().ref('popups').once('value')
            .then((data) => {
              const popups = []
              const obj = data.val()
             

              for (let key in obj) {
                popups.push({
                  id: key,
                  data: obj[key]
                })
              }
              console.log(popups)
              commit('setLoadedPopUps', popups)
            })
            .catch(
              (error) => {
                console.log(error)
              }
            )
            
      },
      username({commit}, payload) {
        commit('setUsername', payload)
      },
      selectedVibe({commit}, payload) {
        commit('setSelectedVibe', payload)
      },
      selectedFontStyle({commit}, payload) {
        commit('setSelectedFontStyle', payload)
      },
      showErrorTiming ({commit}, payload) {
        commit('setShowErrorTiming', payload)
      },
      showErrorUrl({commit}, payload) {
        commit('setShowErrorUrl', payload)
      }
  },
  getters: {
      currentStep (state) {
          return state.currentStep
      },
      currentPreviewStep (state) {
          return state.currentPreviewStep
      },
      name (state) {
          return state.name
      },
      currentPopupType (state) {
          return state.currentPopupType
      },
      currentPopupPosition (state) {
          return state.popupPosition
      },
      currentPopUpAnimation (state) {
          return state.PopUpAnimation
      },
      currentPopUpTitle (state) {
          return state.PopUpTitle
      },
      currentpopupBtnText (state) {
          return state.popupBtnText
      },
      popupBtnColor (state) {
          return state.popupBtnColor
      },
      popUpBtnTextColor (state) {
          return state.popUpBtnTextColor
      },
      currentTextField1 (state) {
          return state.textField1
      },
      currentTextField2 (state) {
          return state.textField2
      },
      currentPopUpSubTitle (state) {
          return state.popUpSubTitle
      },
      showTextField1 (state) {
          return state.showTextField1
      },
      showTextField2 (state) {
          return state.showTextField2
      },
      currentPopupBackgroundColor (state) {
          return state.popupBackgroundColor
      },
      advancedSelected (state) {
          return state.advancedSelected
      },
      popupBtnTextColor (state) {
         return state.popupBtnTextColor
      },
      popUpSubTitleColor (state) {
          return state.popUpSubTitleColor
      },
      popUpTitleColor (state) {
          return state.popUpTitleColor
      },
      showTextField1 (state) {
        return state.showTextField1
      },
      showTextField2 (state) {
        return state.showTextField2
      },
      selectedImage (state) {
        return state.selectedImage
      },
      currentBrowserPageBg (state) {
        return state.browserPageBg
      },
      currentOverlayBg (state){
        return state.overlayBg
      },
      imageUrl(state){
        return state.imageUrl
      },
      currentOverlayBg (state) {
        return state.overlayBg
      },
      currentImageNotes (state) {
         return state.imageNotes
      },
      currentPopUpTiming(state) {
          return state.popupDisplayTiming
      },
      currentWebsiteUrl(state) {
          return state.websiteUrl
      },
      currentfinishingTouchesNotes(state) {
          return state.finishingTouchesNotes
      },
      loading(state) {
        return state.loading
      },
      loadedPopups (state){
        return state.loadedPopups
      },
      loadPopup (state) {
        return (popupId) => {
          return state.loadedPopups.find((popup) => {
            return popup.id === popupId
          })
        }
      },
      showErrorTiming (state) {
        return state.showErrorTiming
      },
      showErrorUrl (state) {
        return state.showErrorUrl
      }
  }
})
